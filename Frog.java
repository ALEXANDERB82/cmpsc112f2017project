import java.awt.Color;
import java.awt.Graphics;

/**
* Creates a frog object that would be used for player or ai
*
*@author Alexander Butler
*
**/
public class Frog{

  /**
  * int x is the frogs starting x coordinate
  * int y is the frogs starting y coordinate
  * Constant SIZE is the size of the frog
  * Constant STEPSIZE is the distance the frog moves with each step
  * boolean hop used to determine whether or not the frog has just moved or not
  * int wait used to calculate the amount of time before the frog can hop again
  **/
  public int x;
  public int y;
  public final int SIZE = 50;
  public final int STEPSIZE = 60;
  private boolean hop;
  private int wait;

  /**
  * Default constructor for the Frog object
  **/
  public Frog(){
    x = 725;
    y = 545;
    hop = false;
    wait = 0;
  }

  /**
  * Determines the way the frog will move in based on an integer provided
  *
  * @param direction used to determine which direction the frog will move(left, right, up, down)
  **/
  public void moveFrog(int direction){
    if(!hop){
      if(direction == 1){//left
        x -= STEPSIZE;
      }
      else if(direction == 2){//right
        x += STEPSIZE;
      }
      else if(direction == 3){//up
        y -= STEPSIZE;
      }
      else if(direction == 4){//down
        y += STEPSIZE;
      }
      hop = true;
    }
    if(x < 5){
      x+= STEPSIZE;
    }
    if(x > 1445){
      x-= STEPSIZE;
    }
    if(y < 5){
      y += STEPSIZE;
    }
    if(y > 545){
      y-= STEPSIZE;
    }
  }

  /**
  * Checks to see if the frog has just hopped and adds to wait it has
  **/
  public void justHopped(){
    if(hop)
      wait++;
    if(wait == 50){
      hop = false;
      wait = 0;
    }
  }

  /**
  * Uses java graphics to paint the frog
  **/
  public void paint(Graphics g){
    g.setColor(Color.GREEN);
    g.fillRect(x,y,SIZE,SIZE);
  }

  /**
  * Gets the frog's current X value
  **/
  public int getX(){
    return x;
  }

  /**
  * Gets the frog's current Y value
  **/
  public int getY(){
    return y;
  }
}

import java.util.ArrayList;
import java.util.Random;

/**
* Creates the lane that the cars are in
*
* @author Alexander Yarkosky
*
**/
public class Lane{

  /**
  * int vehicleType used to set which type of vehicle is in the lane, either 0 for cars or 1 for trucks
  * boolean goingRight used to determine which direction the vehicles are driving towards
  * ArrayList<Vehicle> onTheRoad used to store all of the vehicles currently on the road in the lane
  * int wait used to make there be space between the vehicles by making them wait to add more to the lane
  * boolean waitToSpawnMore used to easily determine if more vehicles can be spawned or not
  * Random r used to create a slight variation in the wait between the vehicles with the random number generated
  * int y used to store the y value of the lane for the creation of the vehicles in the appropriate place
  **/
  private int vehicleType;
  private boolean goingRight;
  public ArrayList<Vehicle> onTheRoad;
  private int wait;
  private boolean waitToSpawnMore;
  private Random r;
  private int y;

  /**
  * Instantiates the Lane object as a default case
  **/
  public Lane(){ //default constructor
    this.vehicleType = 0;
    this.goingRight = false;
    onTheRoad = new ArrayList<Vehicle>();
    wait = 0;
    waitToSpawnMore = false;
    r = new Random();
  }

  /**
  * Instantiates the Lane object with parametes provided
  *
  * @param vehicleType used to get the type of vehicles that will be on the lane
  * @param goingRight used to get which direction the vehicles are headed in
  * @param y used to get where the lane is and therefore where the vehices will spawn
  **/
  public Lane(int vehicleType, boolean goingRight, int y){
    //vehicleTypes: 0 = car, 1 = truck
    this.vehicleType = vehicleType;
    this.goingRight = goingRight;
    this.y = y;
    onTheRoad = new ArrayList<Vehicle>();
    wait = 0;
    waitToSpawnMore = false;
    r = new Random();
  }

  /**
  * Returns the vehicle type of the lane
  *
  * @return the vehicle type with 0 representing cars and 1 representing trucks
  **/
  public int getVehicleType(){
    return vehicleType;
  }

  /**
  * Spawns new vehicles on the road so long as it doesn't have to wait to do so as well
  * as calculating the wait after successfulling spawning a new vehicle
  **/
  public void spawnVehicle(){
    Vehicle newVehicle;
    int extraWait = r.nextInt(25)*10;
    if(!waitToSpawnMore){
      if(vehicleType == 0){
        if(goingRight)
          newVehicle = new Car(-50, y + 5, goingRight);
        else
          newVehicle = new Car(1500, y + 5, goingRight);
        onTheRoad.add(newVehicle);
      }
      if(vehicleType == 1){
        if(goingRight)
          newVehicle = new Truck(-150, y + 5, goingRight);
        else
          newVehicle = new Truck(1500, y + 5, goingRight);
        onTheRoad.add(newVehicle);
      }
      waitToSpawnMore = true;
    }
    else{
      wait++;
      if(vehicleType == 0){
        if(wait >= 200 + extraWait){
          wait = 0;
          waitToSpawnMore = false;
        }
      }
      if(vehicleType == 1){
        if(wait >= 400 + extraWait){
          wait = 0;
          waitToSpawnMore = false;
        }
      }
    }
  }

  /**
  * Checks each vehicle to see if it is outside the bounds of the game and removes
  * them from the road if they are
  **/
  public void removeVehicle(){
    for(int i = 0; i < onTheRoad.size(); i++){
      if(goingRight){
        if(onTheRoad.get(i).getX() >= 1500)
          onTheRoad.remove(i);
      }
      else{
        if(vehicleType == 0){
          if(onTheRoad.get(i).getX() <= -50)
            onTheRoad.remove(i);
        }
        if(vehicleType == 1){
          if(onTheRoad.get(i).getX() <= -150)
            onTheRoad.remove(i);
        }
      }
    }
  }

}

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;
/**
* Generates a color for a cars which are then drawn. Also moves the car at a faster pace than trucks and gets their coordinates to determine if the frog has collided with them
*
* @author Andrew Everitt
*
**/
public class Car extends Vehicle{

  /**
  * int SIZE is used to determine the dimensions of the vehicle when it's drawn
  * int SPEED is used to move the vehicles at a certain pace
  * int x and y are used to find the location of the vehicles on the road
  * int r, g, and b are used to generate the color of the vehicle when spawned
  * int vehicleType is used to determine whether the vehicle is a car or truck
  * boolean goingRight is used to determine what direction the vehicle is moving and changes the x values accordingly
  **/
  final int SIZE = 50;
  final int SPEED = 2;
  int x;
  int y;
  int r, g, b;
  boolean goingRight;
  Color color;

  /**
  * Instantiates the Car object with the provided parameters
  **/
  public Car(int x, int y, boolean goingRight){
    super(x,y);
    this.x = x;
    this.y = y;
    this.goingRight = goingRight;
    color = chooseColor();
  }

  /**
  * Randomly generates values for r, g, and b, which are then used to make a returned color
  *
  * @return the color to be used for a generated vehicle
  **/
  public Color chooseColor(){
    r = (int)(Math.random()*256);
    g = (int)(Math.random()*256);
    b= (int)(Math.random()*256);
    Color color;
    if(r==0 && b==0 && g==0){
      color = new Color (0, 0, 255);
    }
    else {
      color = new Color(r, g, b);
      Random random = new Random();
      final float hue = random.nextFloat();
      final float saturation = 1.0f;
      final float luminance = 1.0f;
      color = Color.getHSBColor(hue, saturation, luminance);
    }
    return color;
  }

  /**
  * Draws the car using the color and SIZE variables
  **/
  public void paint(Graphics g) {
    g.setColor(color);
    g.fillRect(x, y, SIZE, SIZE);
  }

  /**
  * Returns the x value of the car
  *
  * @return the x value that corresponds to how far across the screen the car has moved
  **/
  public double getX() { return x; }

  /**
  * Returns the y value of the car
  *
  * @return the y value that corresponds to the lane that the car is in
  **/
  public double getY() { return y; }

  /**
  * Determines if the car is in a lane where it is moving left to right or vice versa and modifies the x coordinate accordingly
  **/
  public void move() {
    if (goingRight){
      x += SPEED;
    }
    else {
      x -= SPEED;
    }
  }

  /**
  * Determines the x and y coordinates of both the car and frog and compares them to see if an impact has occurred. If it has it returns true.
  *
  * @return true if the frog and a vehicle collide signaling a game over
  **/
  public boolean detectImpact(Frog f, Vehicle v) {
    double fx = f.getX();
    double fy = f.getY();

    double minXCar = v.getX();
    double maxXCar = v.getX() + SIZE;
    double minYCar = v.getY();

    if(goingRight){
      if(minXCar <= fx && fx <= maxXCar && minYCar == fy){
        return true;
      }
    }
    else{
      fx+=50;
      if(minXCar <= fx && fx <= maxXCar && minYCar == fy){
        return true;
      }
    }
    return false;
  }

}

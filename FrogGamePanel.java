import javax.swing.JPanel;
import java.awt.event.KeyEvent;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.util.ArrayList;

/**
* Creates the panel that is used to run and iteract with the game when a player is playing
*
* @author Alexander Yarkosky
**/
public class FrogGamePanel extends JPanel implements Runnable{

  /**
  * final int PANEL_WIDTH used to define how long the display window will be
  * final int PANEL_HEIGHT used to define how tall the display window will be
  * Thread thread used to start the game
  * Frog f used as the frog that plays the game, either player or AI
  * ArrayList<Lane> lanes used to store all of the lanes of vehicles on the road
  * Lane l1, l2, l3, l4, l5, l6 used to create each individual lane and set the
  * kind of vehicle in it, the direction of them, and where it is
  * boolean gameOver used to determine when the frog has either lost or won
  * boolean win usesed to determine when the game is over because the frog won
  * by making it to the otherside of the road
  * boolean playerFrog used to store if it is a player playing as the frog or if
  * it is instead an AI
  * int typeOfAI used to store which AI is being run
  **/
  public final int PANEL_WIDTH = 1500;
  public final int PANEL_HEIGHT = 600;
  private Thread thread;
  private Frog f;
  private ArrayList<Lane> lanes;
  private Lane l1, l2, l3, l4, l5, l6;
  private boolean gameOver;
  private boolean win;
  private boolean playerFrog;
  private int typeOfAI;

  /**
  * Instantiates all nonfinal variables as well as an ArrowListener version of a
  * KeyListener along with setting the background color, defining the dimensions,
  * focusing the thread, and starting the thread as a default case
  **/
  public FrogGamePanel(){
    typeOfAI = 1;
    playerFrog = true;
    if(!playerFrog)
      f = new FrogAI(typeOfAI);
    else
      f = new FrogPlayer();
    lanes = new ArrayList<Lane>();
    l1 = new Lane(1, false, 420); //lane of trucks going left
    l2 = new Lane(0, false, 360); //lane of cars going left
    l3 = new Lane(0, true, 300); //lane of cars going right
    l4 = new Lane(1, false, 240); //lane of trucks going left
    l5 = new Lane(1, true, 180); //lane of trucks going right
    l6 = new Lane(0, true, 120); //lane of cars going right
    lanes.add(l1);
    lanes.add(l2);
    lanes.add(l3);
    lanes.add(l4);
    lanes.add(l5);
    lanes.add(l6);
    gameOver = false;
    win = false;
    this.addKeyListener(new ArrowListener(f));
    this.setBackground(Color.BLACK);
    this.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
    this.setFocusable(true);
    thread = new Thread(this);
    thread.start();
  }

  /**
  * Instantiates all nonfinal variables as well as an ArrowListener version of a
  * KeyListener along with setting the background color, defining the dimensions,
  * focusing the thread, and starting the thread when the information of whether
  * an AI is controlling the frog and which AI that would be
  **/
  public FrogGamePanel(boolean playerFrog, int typeOfAI){
    this.typeOfAI = typeOfAI;
    this.playerFrog = playerFrog;
    if(!playerFrog)
      f = new FrogAI(typeOfAI);
    else
      f = new FrogPlayer();
    lanes = new ArrayList<Lane>();
    l1 = new Lane(1, false, 420); //lane of trucks going left
    l2 = new Lane(0, false, 360); //lane of cars going left
    l3 = new Lane(0, true, 300); //lane of cars going right
    l4 = new Lane(1, false, 240); //lane of trucks going left
    l5 = new Lane(1, true, 180); //lane of trucks going right
    l6 = new Lane(0, true, 120); //lane of cars going right
    lanes.add(l1);
    lanes.add(l2);
    lanes.add(l3);
    lanes.add(l4);
    lanes.add(l5);
    lanes.add(l6);
    gameOver = false;
    win = false;
    this.addKeyListener(new ArrowListener(f));
    this.setBackground(Color.BLACK);
    this.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
    this.setFocusable(true);
    thread = new Thread(this);
    thread.start();
  }
  /**
  * Creates the visual display of the objects in the game as well as the
  * game over screens for both winning and losing
  **/
  public void paintComponent(Graphics g){
    super.paintComponent(g);
    if(gameOver){
      if(win){
        //congrations you won screen
        g.setColor(Color.ORANGE);
        g.setFont(new Font("Courier", Font.BOLD, 50));
        g.drawString("YOU WON!", 625, 300);
      }
      else{
        //game over you lose screen
        g.setColor(Color.RED);
        g.setFont(new Font("Courier", Font.BOLD, 50));
        g.drawString("YOU LOSE", 650, 300);
      }
    }
    else{
      g.setColor(new Color(35, 105, 15)); //grass color
      g.fillRect(0, 480, 1500, 120); //lower grass area for start
      g.fillRect(0, 0, 1500, 120); //upper grass area fo finish
      for(int i = 0; i < 6; i++){
        for(int j = 0; j < lanes.get(i).onTheRoad.size(); j++){
          lanes.get(i).onTheRoad.get(j).paint(g);
        }
      }
      f.paint(g);
    }
  }
  /**
  * Updates the game by moving the AI frog, determining when the frog has
  * just moved/hopped, moves the vehicles, creates new vehicles, repaints
  * the objects that have moved at their new locations, and then checks if
  * the game is over
  **/
  public void run(){
    for(;;){
      if(!playerFrog)
        f.moveFrog(0);
      f.justHopped();
      moveVehicles();
      generateVehicles();
      checkVehicleBounds();
      repaint();
      checkGameOver();
      if(gameOver)
        break;
      try{
        Thread.sleep(15);
      } catch(InterruptedException e){
        e.printStackTrace();
      }
    }
  }
  /**
  * Checks to see if the frog has either lost by getting hit by a vehicle or
  * won by making it to across the road to the other side and then appropriately
  * setting the values of the gameOver and win variables
  **/
  public void checkGameOver(){
    //check for collision between frog and vehicle, then set gameOver = true;
    //check to see if the frog made it across the rode then set gameOver = true; and win = true;
    if(f.getY() < 120){
      win = true;
      gameOver = true;
      return;
    }
    for(int i = 0; i < 6; i++){
      for(int j = 0; j < lanes.get(i).onTheRoad.size(); j++){
        if(lanes.get(i).onTheRoad.get(j).detectImpact(f, lanes.get(i).onTheRoad.get(j)) == true){
          gameOver = true;
          return;
        }
      }
    }
  }

  /**
  * Creates the vehicles that will be on the road for each lane via the method
  * in the Lane class that determines when it is appropriate for another vehicle
  * to be put on the road
  **/
  public void generateVehicles(){
    for(int i = 0; i < 6; i++){
      lanes.get(i).spawnVehicle();
    }
  }

  /**
  * Checks to see if the vehicles on the road are within the bounds of the game
  * for each lane via the method in the Lane class that checks this and then removes
  * them from the road if they are in fact out of the bounds
  **/
  public void checkVehicleBounds(){
    for(int i = 0; i < 6; i++){
      lanes.get(i).removeVehicle();
    }
  }

  /**
  * Moves each individual vehicle on each individual lane by calling that vehicle's
  * method of the same name in order to update its position
  **/
  public void moveVehicles(){
    for(int i = 0; i < 6; i++){
      for(int j = 0; j < lanes.get(i).onTheRoad.size(); j++){
        lanes.get(i).onTheRoad.get(j).move();
      }
    }
  }

}

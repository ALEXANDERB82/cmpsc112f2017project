import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

/**
* Used as a KeyListener to detect when the player is having the frog move while playing
**/
public class ArrowListener implements KeyListener{

  /**
  * Frog f used as the Frog that is being affected by the keys
  **/
  Frog f;
  
  /**
  * Constructor that takes in the Frog to be moved
  **/
  public ArrowListener(Frog f){
    this.f = f;
  }
  
  /**
  * Moves the player's frog based off of the corresponding key codes of the arrow keys to
  * move left, right, up, and down respectively
  **/
  @Override
  public void keyPressed(KeyEvent e){
    int direction = 1;
      if(e.getKeyCode() == 37){
        direction = 1;
      }
      if(e.getKeyCode() == 39){
        direction = 2;
      }
      if(e.getKeyCode() == 38){
        direction = 3;
      }
      if(e.getKeyCode() == 40){
        direction = 4;
      }
      f.moveFrog(direction);
  }

  /**
  * not used
  **/
  @Override
  public void keyReleased(KeyEvent e){
  }

  /**
  * not used
  **/
  @Override
  public void keyTyped(KeyEvent e){
  }

}

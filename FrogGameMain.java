import javax.swing.JFrame;
import java.util.Scanner;

/**
* Creates the main that runs and displays the FrogGamePanel
*
* @author Alexander Yarkosky
*
**/
public class FrogGameMain{
  /**
  * Asks the user before the game if they want to play or if they want to run an
  * AI and if so, which one and then creates the JFrame that runs and displays the
  * the game as defined by the FrogGamePanel
  *
  * @param args
  **/
  public static void main(String[] args){
    boolean playerFrog = true;
    int aiType = 1;
    String input;
    Scanner s = new Scanner(System.in);
    System.out.print("Would you like to run an AI?(y OR n): ");
    input = s.next();
    if(input.toLowerCase().substring(0,1).equals("y")){
      playerFrog = false;
      System.out.print("Choose which AI you would like to run(1 OR 2): ");
      aiType = s.nextInt();
      while(!(aiType == 1 || aiType == 2)){
        System.out.print("You entered " + aiType + ". Please enter either 1 or 2: ");
        aiType = s.nextInt();
      }
    }
    JFrame frame = new JFrame("Frog Game");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.getContentPane().add(new FrogGamePanel(playerFrog, aiType));
    frame.pack();
    frame.setVisible(true);
  }
}

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

/**
* Creates a frog object that would be used for two different AI
*
*@author Alexander Butler
*
**/
public class FrogAI extends Frog{

  /**
  * int x is the frogs starting x coordinate
  * int y is the frogs starting y coordinate
  * Constant SIZE is the size of the frog
  * Constant STEPSIZE is the distance the frog moves with each step
  * boolean hop used to determine whether or not the frog has just moved or not
  * int wait used to calculate the amount of time before the frog can hop again
  * int ai determines the type of ai
  **/
  public int x;
  public int y;
  public final int SIZE = 50;
  public final int STEPSIZE = 60;
  private boolean hop;
  private int wait;
  private int ai;

  /**
  * Default constructor for the FrogAI object
  * @param ai used to get the type of ai that will be created
  **/
  public FrogAI(int ai){
    x = 700;//tentative
    y = 545;//tentative
    this.ai = ai;
    hop = false;
    wait = 0;
  }

  /**
  * Determines the way that each frog AI will move
  *
  * @param useless Used as an easy way to implement the method override
  **/
  public void moveFrog(int useless){
    if(!hop){
      switch(ai){
        case 1:
          int waitToStart;
          waitToStart = randomWait();
          if(waitToStart < 50){
            while(waitToStart != 400){
              waitToStart++;
            }
          }
          else{
            y -= STEPSIZE;
          }
          break;
        case 2:
          switch(generateRandom()){
            case 1:
              x -= STEPSIZE;
              break;
            case 2:
              x += STEPSIZE;
              break;
            case 3:
              y -= STEPSIZE;
              break;
          }
          break;
        default:
          x -= STEPSIZE;
          x += STEPSIZE;
          y += STEPSIZE;
          y -= STEPSIZE;
          break;
      }
    }
    hop = true;
    if(x < 5){
      x+= STEPSIZE;
    }
    if(x > 1445){
      x-= STEPSIZE;
    }
    if(y < 5){
      y += STEPSIZE;
    }
    if(y > 545){
      y-= STEPSIZE;
    }
  }

  /**
  * Generates a raondom number to determine the second AI's direction of movement
  **/
  public int generateRandom(){
    Random rand = new Random();
    int a = rand.nextInt(3)+1;
    return a;
  }

  /**
  * Generates a raondom number to determine the firt AI's extra wait time before movement
  **/
  public int randomWait(){
    Random rand2 = new Random();
    int b = rand2.nextInt(40)*10;
    return b;
  }

  /**
  * Checks to see if the frog has just hopped and adds to wait it has
  **/
  public void justHopped(){
    if(hop)
      wait++;
    if(wait == 50){
      hop = false;
      wait = 0;
    }
  }

  /**
  * Uses java graphics to paint the frog
  **/
  public void paint(Graphics g){
    g.setColor(Color.GREEN);
    g.fillRect(x,y,SIZE,SIZE);
  }

  /**
  * Gets the frog's current X value
  **/
  public int getX(){
    return x;
  }

  /**
  * Gets the frog's current Y value
  **/
  public int getY(){
    return y;
  }
}
